package table.model;

/**
 * カラム情報項目クラス
 * @author onodera
 *
 */
public class Column {

	private String logicName;
	private String physicsName;
	private String dateType;
	private String notNULL;
	private String notes="";

	/**
	 * コンストラクタ
	 */
	public Column(){
		logicName="";
		physicsName="";
		dateType="";
		notNULL="";
		notes="";
	}

	/**
	 * コンストラクタオーバーロード
	 * @param logicName カラム項目論理名
	 * @param physicsName カラム項目物理名
	 * @param dateType データタイプ
	 * @param notNull NULL不可
	 * @param notes 備考
	 */
	public Column(String logicName,String physicsName,String dateType,String notNull,String notes){
		this.logicName=logicName;
		this.physicsName=physicsName;
		this.dateType=dateType;
		this.notNULL=notNull;
		this.notes=notes;
	}

	/**
	 * カラム項目論理名getter
	 * @return カラム項目論理名
	 */
	public String getLogicName() {
		return logicName;
	}

	/**
	 * カラム項目論理名setter
	 * @param logicName カラム項目論理名
	 */
	public void setLogicName(String logicName) {
		this.logicName = logicName;
	}

	/**
	 * カラム項目物理名getter
	 * @return カラム項目物理名
	 */
	public String getPhysicsName() {
		return physicsName;
	}

	/**
	 * カラム項目論理名setter
	 * @param physicsName カラム項目論理名
	 */
	public void setPhysicsName(String physicsName) {
		this.physicsName = physicsName;
	}

	/**
	 * データタイプgetter
	 * @return データタイプ
	 */
	public String getDateType() {
		return dateType;
	}

	/**
	 * データタイプsetter
	 * @param dateType データタイプ
	 */
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	/**
	 * NULL不可getter
	 * @return NULL不可
	 */
	public String getNotNULL() {
		return notNULL;
	}

	/**
	 * NULL不可setter
	 * @param notNULL NULL不可
	 */
	public void setNotNULL(String notNULL) {
		this.notNULL = notNULL;
	}

	/**
	 * 備考getter
	 * @return 備考
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * 備考setter
	 * @param notes 備考
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String toString(){
		return logicName+","+physicsName+","+dateType+","+notNULL+","+notes;
	}


}
