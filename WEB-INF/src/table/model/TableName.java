package table.model;

/**
 * テーブル論理名、物理名クラス
 * @author onodera
 *
 */
public class TableName {
	private String tableLogic;
	private String tablePhysics;
	/**
	 * コンストラクタ
	 */
	public TableName(){
		tableLogic = "";
		tablePhysics = "";
	}
	/**
	 * テーブル論理名getter
	 * @return テーブル論理名
	 */
	public String getTableLogic() {
		return tableLogic;
	}
	/**
	 * テーブル論理名setter
	 * @param tableLogic テーブル論理名
	 */
	public void setTableLogic(String tableLogic) {
		this.tableLogic = tableLogic;
	}
	/**
	 * テーブル物理名getter
	 * @return テーブル物理名
	 */
	public String getTablePhysics() {
		return tablePhysics;
	}
	/**
	 * テーブル物理名setter
	 * @param tablePhysics テーブル物理名
	 */
	public void setTablePhysics(String tablePhysics) {
		this.tablePhysics = tablePhysics;
	}
	/**
	 * コンストラクオーバーロード
	 * @param tableLogic テーブル論理名
	 * @param tablePhysics テーブル物理名
	 */
	public TableName(String tableLogic,String tablePhysics){
		this.tableLogic = tableLogic;
		this.tablePhysics = tablePhysics;
	}
	public String toString(){
		return tableLogic+","+tablePhysics;
	}
}
