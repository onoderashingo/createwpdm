package table.model;

import java.util.ArrayList;

/**
 * テーブル論理名、物理名格納リストクラス
 * @author onodera
 *
 */
public class TableNameList {
	private ArrayList<TableName> tableNameList;
	/**
	 * コンストラクタ
	 */
	public TableNameList(){
		this.tableNameList = new ArrayList<TableName>();
	}
	/**
	 * 初期化
	 * @return
	 */
	public ArrayList<TableName> tableNameList(){
		this.tableNameList = new ArrayList<TableName>();
		return tableNameList;
	}
}
