package table.model;

import java.util.ArrayList;

/**
 * カラム情報項目格納リストクラス
 * @author onodera
 *
 */
public class ColumnList {
	private ArrayList<Column> columnList;
	/**
	 * コンストラクタ
	 */
	public ColumnList(){
		this.columnList = new ArrayList<Column>();
	}
	/**
	 * 初期化
	 * @return
	 */
	public ArrayList<Column> columnList(){
		this.columnList = new ArrayList<Column>();
		return columnList;
	}
}
