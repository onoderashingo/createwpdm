package table.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import table.control.ExcelRead;
import table.control.SheetRead;

/**
 * テーブル定義書Excelファイルアップロード、読込、 テーブル名、カラム情報リスト作成サーブレット
 *
 * @author onodera
 *
 */
public class TableServlet extends HttpServlet {

	private String file = null;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain; charset=UTF-8");

		PrintWriter out = response.getWriter();
		// 読込除外シート名テキストファイルパス
		String exceptPath = getServletContext().getRealPath("/except");
		Path except = Paths.get(exceptPath + "/except.txt");
		// Excelファイル一時フォルダのパス
		String filepath = getServletContext().getRealPath("/files");

		XSSFWorkbook book = null;
		HttpSession sess = null;

		try {
			// 読込除外シート名テキストファイル読込
			List<String> lines = Files.readAllLines(except, StandardCharsets.UTF_8);

			DiskFileItemFactory factory = new DiskFileItemFactory();
			// 一時ファイル
			factory.setRepository(new File("/tmp"));
			ServletFileUpload upload = new ServletFileUpload(factory);
			factory.setSizeThreshold(1024);
			upload.setSizeMax(-1);
			upload.setHeaderEncoding("UTF-8");

			// formデータ読み込み
			List<FileItem> list = upload.parseRequest(request);

			// Excelファイル読込、アップロード
			ArrayList<Object> ret = ExcelRead.Excel(list, filepath);
			book = (XSSFWorkbook)ret.get(0);
			file = (String)ret.get(1);

			// テーブル定義書名リスト、カラム項目リストマップ取得
			ArrayList<Object> o = new ArrayList<Object>();
			o = (ArrayList<Object>) SheetRead.sheetread(book, lines);

			sess = request.getSession();
			sess.setAttribute("tableList", o.get(0));
			sess.setAttribute("tableMap", o.get(1));
			sess.setAttribute("targetKey",o.get(2));

			response.sendRedirect("./page/UpLoad.jsp");

		} catch (NotOfficeXmlFileException e) {
			out.println("テーブル定義書(xlsxファイル)以外のファイルが選択されています。");
		} catch (NullPointerException e) {
			out.println("テーブル定義書ファイルが空です。");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (book != null) {
				try {
					book.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		Path path = Paths.get(file);
		Files.delete(path);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext app = this.getServletContext();
		RequestDispatcher rd = app.getRequestDispatcher("/page/UpLoad.jsp");
		rd.forward(request, response);
	}
}
