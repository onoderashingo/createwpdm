package table.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import table.model.Column;



/**
 * テーブル名変更時、Javasciptから呼び出される
 * カラム情報リスト更新サーブレット
 * @author onodera
 *
 */
public class JsonServlet extends HttpServlet{
	private final String REQUEST_STRING = "targetKey";

	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		response.setContentType("application/json;charset=UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "http://localhost");
		String targetKey = null;

		try{
		//選択されたテーブル物理名を取得
		targetKey = request.getParameter(REQUEST_STRING);
		HttpSession sess = request.getSession();
		sess.setAttribute("targetKey", targetKey);

		//カラム情報マップを取得
		Map<String,ArrayList<Column>> tableMap = new LinkedHashMap<String,ArrayList<Column>>();
		tableMap = (Map<String,ArrayList<Column>>)sess.getAttribute("tableMap");

		//テーブル物理名に所属しているカラム情報をリストに格納
		ArrayList<Column> columnList = new ArrayList<Column>();
		columnList = tableMap.get(targetKey);
		sess.setAttribute("columnList", columnList);

		response.sendRedirect("./page/UpLoad.jsp");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		ServletContext app = this.getServletContext();
		RequestDispatcher rd = app.getRequestDispatcher("/page/UpLoad.jsp");
		rd.forward(request, response);
	}
}
