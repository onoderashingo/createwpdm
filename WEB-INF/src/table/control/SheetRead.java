package table.control;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import table.model.Column;
import table.model.TableName;

/**
 * Excelシート読込クラス
 * @author onodera
 *
 */
public class SheetRead {

	/**
	 * Excelシート読込メソッド
	 * @param book Excelファイル
	 * @param lines 読込除外シート名
	 * @return テーブル物理名リスト、カラムリストマップ
	 */
	public static ArrayList<Object> sheetread(XSSFWorkbook book, List<String> lines) {
		Map<String, ArrayList<Column>> tableMap = new LinkedHashMap<String, ArrayList<Column>>();
		ArrayList<TableName> tableNameList = new ArrayList<TableName>();
		ArrayList<Object> o = new ArrayList<Object>();
		String targetKey = "";

		try {
			int count = 0;
			// シート読み込み
			for (int i = 0; i < book.getNumberOfSheets(); ++i) {
				XSSFSheet sheet = (XSSFSheet) book.getSheetAt(i);
				TableName tableName = new TableName();

				// シート名が除外ファイルリストに存在するか判定
				if (!(lines.contains(sheet.getSheetName()))) {
					count++;

					// 値読み込み
					// 論理テンティティ名取得
					tableName = CellRead.entity(sheet, tableName, 4, 2);
					// 物理エンティティ名取得
					tableName = CellRead.entity(sheet, tableName, 5, 2);

					// 最初に表示するテーブルの物理名をセット
					if (count == 1) {
						targetKey = tableName.getTablePhysics();
						}

					// 論理、物理エンティティをシートごとにリストに格納
					tableNameList.add(new TableName(tableName.getTableLogic(), tableName.getTablePhysics()));

					// カラム情報取得
					ArrayList<Column> columnList = new ArrayList<Column>();
					columnList = CellRead.columnRead(sheet);
					// ハッシュマップにカラムリストを格納
					tableMap.put(tableName.getTablePhysics(), columnList);
				}
			}
			o.add((Object)tableNameList);
			o.add((Object)tableMap);
			o.add((Object)targetKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return o;
	}
}
