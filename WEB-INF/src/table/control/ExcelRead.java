package table.control;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServlet;

import org.apache.commons.fileupload.FileItem;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Excelファイル読込クラス
 * @author onodera
 *
 */
public class ExcelRead extends HttpServlet {
	/**
	 * Excelファイル読込、アップロードメソッド
	 * @param list サーブレットで読み込んだFormデータ
	 * @param filepath Excel一時アップロードディレクトリパス
	 * @return
	 */
	public static ArrayList<Object> Excel(List<FileItem> list,String filepath) {
		ArrayList<Object> ret = new ArrayList<Object>();
		String file = "";
		FileItem fItem = null;
		XSSFWorkbook book = null;
		FileInputStream filein = null;
		try {
			// listに展開
			Iterator<FileItem> iterator = list.iterator();
			while (iterator.hasNext()) {
				fItem = (FileItem) iterator.next();
				if (!(fItem.isFormField())) {
					String fileName = fItem.getName();
					// Excelデータをfilesフォルダにアップロード
					if ((fileName != null) && (!(fileName.equals("")))) {
						fileName = (new File(fileName)).getName();
						file = filepath + "/" + fileName;
						fItem.write(new File(file));
					}
					// Excelファイル読み込み
					filein = new FileInputStream(file);
					book = new XSSFWorkbook(filein);
				}
				ret.add((Object)book);
				ret.add((Object)file);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fItem != null) {
				try {
					fItem.delete();
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (book != null) {
					try {
						book.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (filein != null) {
					try {
						filein.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return ret;
	}
}
