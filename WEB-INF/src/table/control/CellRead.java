package table.control;

import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import table.model.Column;
import table.model.TableName;

/**
 * セル読込メソッドクラス
 * @author onodera
 *
 */
public class CellRead {
	static String cellString = "";

	// Excelデータ変換メソッド
	/**
	 * Excelデータ変換メソッド
	 *
	 * @param cell
	 *            Excelファイルの対象セル
	 * @return 読み込んだセルの内容を返す
	 */
	public static String cellRead(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			cellString = cell.getStringCellValue();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			cellString = Double.toString(cell.getNumericCellValue());
			break;
		}
		return cellString;
	}

	// 論理エンティティ、物理エンティティ取得メソッド
	/**
	 * テーブル定義書論理エンティティ名、物理エンティティ名取得メソッド
	 *
	 * @param sheet
	 *            Excelの対象シート
	 * @param tableName
	 *            テーブル定義書名インスタンス
	 * @param RowNum
	 *            行番号
	 * @param CellNum
	 *            列番号
	 * @throws Exception
	 *
	 * @return テーブル定義書名インスタンス
	 */
	public static TableName entity(XSSFSheet sheet,TableName tableName, int RowNum, int CellNum) throws Exception {
		cellString = null;
		Row row = sheet.getRow(RowNum);
		Cell cell = row.getCell(CellNum);
		if (cell == null) {
			cellString = "";
		}
		cellString = (cellRead(cell));
		if (cellString == null) {
			cellString = "";
		}
		if (RowNum == 4) {
			tableName.setTableLogic(cellString);
		} else if (RowNum == 5) {
			tableName.setTablePhysics(cellString);
		}
		return tableName;
	}

	// カラム情報1シート取得メソッド
	/**
	 * シートずつのカラム情報取得メソッド
	 *
	 * @param sheet
	 *            Excelの対象シート
	 * @return カラム情報を読み込んだリストを返す
	 */
	public static ArrayList<Column> columnRead(XSSFSheet sheet) {
		ArrayList<Column> columnList = new ArrayList<Column>();
		for (int rowIdx = 13; rowIdx <= sheet.getLastRowNum(); rowIdx++) {
			cellString = null;
			Column column = new Column();
			Row row = sheet.getRow(rowIdx);
			if (row == null) {
				break;
			}
			for (int colIdx = 1; colIdx <= 6; colIdx++) {
				// array2 = new ArrayList<String>();
				cellString = null;
				Cell cell = row.getCell(colIdx);
				if (cell == null) {
					continue;
				}
				cellString = cellRead(cell);
				if (cellString == null) {
					// cellString = "";
					continue;
				}
				// カラム情報項目をリストに格納
				switch (colIdx) {
				case 1:
					column.setLogicName(cellString);
					break;
				case 2:
					column.setPhysicsName(cellString);
					break;
				case 3:
					column.setDateType(cellString);
					break;
				case 4:
					column.setNotNULL(cellString);
					break;
				case 6:
					column.setNotes(cellString);
					break;
				default:
					break;
				}
			}
			columnList.add(new Column(column.getLogicName(), column.getPhysicsName(), column.getDateType(),
					column.getNotNULL(), column.getNotes()));
		}
		return columnList;
	}
}
