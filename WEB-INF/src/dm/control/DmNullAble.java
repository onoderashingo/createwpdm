package dm.control;

import dm.model.DataModel;

public class DmNullAble {
	/**
	 * データモデルNull可取得メソッド
	 * @param dm データモデルインスタンス
	 * @param notNull テーブル定義書NOT NULL項目
	 * @return データモデル
	 */
	public static DataModel getDmNullAble(DataModel dm,String notNull){
		// Null可セット
		dm.setNullAble("◯");
		// PKが設定されていれば主キーセット
		if (notNull != null && !(notNull.equals(""))) {
			String regex = "PK";
			if (notNull.indexOf(regex) != -1) {
				dm.setPrimaryKey("◯");
			}
			// 入力値があればNull不可にセット
			dm.setNullAble("");
		}
		return dm;
	}
}
