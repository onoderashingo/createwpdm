package dm.control;

import dm.model.DataModel;

public class DmName {
	/**
	 * データモデル論理名、物理名、targetAlias取得メソッド
	 * @param dmName データモデルインスタンス
	 * @return データモデル
	 */
	public static DataModel getDmName(DataModel dmName) {
		// 論理名が空の場合、物理名を設定
		if (dmName.getLogicName() == null || dmName.getLogicName().equals("")) {
			dmName.setLogicName(dmName.getPhysicsName());
		} else {
			dmName.setAlias(dmName.getPhysicsName());
		}
		return dmName;
	}
}
