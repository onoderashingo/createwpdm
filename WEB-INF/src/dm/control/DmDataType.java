package dm.control;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dm.model.DataModel;

public class DmDataType {
	static String dataType = "";
	static int stringValue = 0;

	static String dataTypeSW ="";
	static Pattern p = null;
	static Matcher m = null;
	// データタイプ変換用配列
	static final String[] nvarcharList = { "nvarchar", "varchar", "nchar", "char"  };// 文字列
	/**
	 * データモデルデータタイプ取得、変換メソッド
	 * @param dm データモデルインスタンス
	 * @return データモデル
	 */
	public static DataModel getDmDataType(DataModel dm){
		//navarchar,char系統の場合
		String dataType = dm.getDataType();
		match(dataType,"([a-zA-Z]*+)(\\S)");
		if (m.find()) {
			dataTypeSW = m.group(1);
		} else {
			// identityが設定されている場合
			match(dataType,"([a-zA-Z]*+)(\\D)");
			if (m.find()) {
				dataType = m.group(1);
			} else{
				dataTypeSW = dataType;
			}
		}
		// nvarchar,nchar系統の場合、桁数を取得。
		if (Arrays.asList(nvarcharList).contains(dataTypeSW)) {
			match(dataType,"(\\D+)([0-9]+)(\\D)");
			// 桁数セット
			if (m.find()) {
				stringValue = Integer.parseInt(m.group(2));
				dm.setStringValue(stringValue);
				dataType = "nvarchar";
			} else {
				// maxの場合500をセット
				match(dataType,"(\\D+)(max|MAX+)(\\D)");
				if (m.find()) {
					stringValue = 500;
					dm.setStringValue(stringValue);
					dataType = "text";
				} else {
					dataType = "nvarchar";
				}
			}
		}
		if (dataType == null || dataType.equals("")) {
			dataType = "";
		} else {
			// DM項目データタイプ変換
			dmChange(dm, dataType);
		}
		return dm;
	}

	/**
	 * DM項目データタイプ変換メソッド
	 *
	 * @param dm
	 *            データモデルインスタンス
	 * @param dataType
	 *            テーブル定義書データタイプ
	 * @return 戻り値：データモデルインスタンス
	 */
	public static DataModel dmChange(DataModel dm, String dataType) {
		switch (dataType) {
		case "int":
		case "money":
			dm.setDataType("NUM");
			break;
		case "nvarchar":
			dm.setDataType("CODE");
			break;
		case "text":
			dm.setDataType("TEXT");
			dm.setStringValue(500);
			break;
		case "tinyint":
		case "bit":
			dm.setDataType("NUM");
			dm.setStringValue(1);
			dm.setTinyintFlag(1);
			break;
		case "date":
			dm.setDataType("DATE");
			dm.setStringValue(0);
			break;
		case "datetime":
			dm.setDataType("TIME");
			dm.setStringValue(0);
			break;
		case "file":
			dm.setDataType("FILE");
			dm.setStringValue(0);
			break;
		default:
			dm.setDataType("");
			break;
		}
		return dm;
	}

	/**
	 * 文字列マッチングメソッド
	 * @param dataType 検索対象文字列
	 * @param regex 検索文字列
	 * @return マッチンググループ
	 */
	public static Matcher match(String dataType,String regex){
		p = Pattern.compile(regex);
		m = p.matcher(dataType);
		return m;
	}
}
