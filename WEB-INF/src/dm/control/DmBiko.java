package dm.control;

import dm.model.DataModel;

public class DmBiko {
	/**
	 * データモデル備考取得、変換メソッド
	 * @param dm データモデルインスタンス
	 * @param notes 備考
	 * @return データモデル
	 */
	public static DataModel getDmbiko(DataModel dm ,String notes){

		String[] note = {};
		// 備考欄を,で区切る
		note = notes.split(",", 0);
		for (int s = 0; s < note.length; s++) {
			if (note[s] != null) {
				String[] noteValue = {};
				// :で区切る
				noteValue = note[s].split(":", 0);
				// データタイプの処理
				if (noteValue[0].equals("type")) {
					dm.setDataType(noteValue[1]);
				}
				// データタイプが桁数(length)の処理。
				else if (("NUM".equals(dm.getDataType()) || "CODE".equals(dm.getDataType()) || "TEXT".equals(dm.getDataType())) && noteValue[0].equals("length") && dm.getTinyintFlag()!=1) {
					dm.setStringValue(Integer.parseInt(noteValue[1]));
				}
			}
		}
		return dm;
	}
}
