package dm.control;

import dm.model.DataModel;

public class DmColumnName {
	/**
	 * データモデル項目物理名、論理名、columnAlias取得メソッド
	 * @param dm データモデルインスタンス
	 * @return データモデル
	 */
	public static DataModel getDmColumnName(DataModel dm){
		// 論理名が空の場合、物理名を設定
		if (dm.getLogicName() == null || dm.getLogicName().equals("")) {
			dm.setLogicName(dm.getPhysicsName());
		} else {
			dm.setAlias(dm.getPhysicsName());
		}
		return dm;
	}
}
