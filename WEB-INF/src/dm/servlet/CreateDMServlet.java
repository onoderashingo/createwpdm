package dm.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dm.control.DmBiko;
import dm.control.DmColumnName;
import dm.control.DmDataType;
import dm.control.DmName;
import dm.control.DmNullAble;
import dm.model.DataModel;

/**
 * テーブル定義書からデータモデル項目変換サーブレット
 * @author onodera
 *
 */
public class CreateDMServlet extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain; charset=UTF-8");
		HttpSession sess = request.getSession();
		try {
			//テーブル名取得
			DataModel dmName = new DataModel();
			dmName.setLogicName(request.getParameter("TableLogic"));
			dmName.setPhysicsName(request.getParameter("TablePhysics"));
			dmName=DmName.getDmName(dmName);

			// テーブル項目取得、DM項目コード変換
			ArrayList<DataModel> dataModelList = new ArrayList<DataModel>();
			String[] nums = request.getParameterValues("num");
			for (int i = 0; i < nums.length; i++) {
				DataModel dm = new DataModel();

				// 論理名、物理名取得
				dm.setLogicName(request.getParameter("logicName" + nums[i]));
				dm.setPhysicsName(request.getParameter("physicsName" + nums[i]));
				dm=DmColumnName.getDmColumnName(dm);

				// データタイプ取得
				dm.setDataType(request.getParameter("dataType" + nums[i]).toLowerCase());
				dm=DmDataType.getDmDataType(dm);

				//Null可セット
				String notNull=(request.getParameter("notNull" + nums[i]));
				dm=DmNullAble.getDmNullAble(dm,notNull);

				// 備考取得
				String notes = request.getParameter("notes" + nums[i]);
				dm=DmBiko.getDmbiko(dm, notes);

				// リストに格納
				dataModelList.add(new DataModel(dm.getLogicName(), dm.getPhysicsName(), dm.getAlias(), dm.getNullAble(),
						dm.getPrimaryKey(), dm.getStringValue(), dm.getDataType(),dm.getTinyintFlag()));
			}
			sess.setAttribute("dmName", dmName);
			sess.setAttribute("dataModelList", dataModelList);
			response.sendRedirect("./page/CreateDM.jsp");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
