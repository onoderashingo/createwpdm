package dm.model;

/**
 * データモデル項目クラス
 * @author onodera
 *
 */
public class DataModel {
	private String logicName;
	private String physicsName;
	private String alias;
	private String nullAble;
	private String primaryKey;
	private int stringValue;
	private String dataType;
	private int tinyintFlag;

	/**
	 * コンストラクタ
	 */
	public DataModel(){
		logicName="";
		physicsName="";
		alias="";
		nullAble="";
		primaryKey="";
		stringValue=0;
		dataType="";
		tinyintFlag=0;
	}
	/**
	 * コンストラクタオーバーロード
	 * @param logicName データモデル項目論理名
	 * @param physicsName データモデル項目物理名
	 * @param alias データモデルcolumnAlias名
	 * @param nullAble NULL許可
	 * @param primaryKey キーグループ
	 * @param stringVlue 桁数
	 * @param dataType データタイプ
	 * @param tinyintFlag tinyintフラグ
	 */
	public DataModel(String logicName,String physicsName,String alias,String nullAble,String primaryKey, int stringVlue,String dataType,int tinyintFlag){
		this.logicName=logicName;
		this.physicsName=physicsName;
		this.alias=alias;
		this.nullAble=nullAble;
		this.primaryKey=primaryKey;
		this.stringValue=stringVlue;
		this.dataType=dataType;
		this.tinyintFlag=tinyintFlag;
	}

	/**
	 * データモデル項目論理名getter
	 * @return データモデル項目論理名
	 */
	public String getLogicName() {
		return logicName;
	}

	/**
	 * データモデル項目論理名setter
	 * @param logicName データモデル項目論理名
	 */
	public void setLogicName(String logicName) {
		this.logicName = logicName;
	}
	/**
	 * データモデル項目物理名getter
	 * @return データモデル項目物理名
	 */
	public String getPhysicsName() {
		return physicsName;
	}
	/**
	 * データモデル項目物理名setter
	 * @param physicsName データモデル項目物理名
	 */
	public void setPhysicsName(String physicsName) {
		this.physicsName = physicsName;
	}

	/**
	 * columnAlias名setter
	 * @return columnAlias名
	 */
	public String getAlias() {
		return alias;
	}
	/**
	 * columnAlias名getter
	 * @param alias columnAlias名
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * NULL許可getter
	 * @return NULL許可
	 */
	public String getNullAble() {
		return nullAble;
	}
	/**
	 * NULL許可setter
	 * @param nullAble NULL許可
	 */
	public void setNullAble(String nullAble) {
		this.nullAble = nullAble;
	}
	/**
	 * キーグループgetter
	 * @return キーグループ
	 */
	public String getPrimaryKey() {
		return primaryKey;
	}
	/**
	 * キーグループsetter
	 * @param primaryKey キーグループ
	 */
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	/**
	 * 桁数getter
	 * @return 桁数
	 */
	public int getStringValue() {
		return stringValue;
	}
	/**
	 * 桁数setter
	 * @param stringValue 桁数
	 */
	public void setStringValue(int stringValue) {
		this.stringValue = stringValue;
	}
	/**
	 * データタイプsetter
	 * @return データタイプ
	 */
	public String getDataType() {
		return dataType;
	}
	/**
	 * データタイプgetter
	 * @param dataType データタイプ
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	/**
	 * tinyintフラグgetter
	 * @return tinyintFlag tinyintフラグ
	 */
	public int getTinyintFlag() {
		return tinyintFlag;
	}
	/**
	 * tinyintフラグsetter
	 * @param tinyintFlag tinyintフラグ
	 */
	public void setTinyintFlag(int tinyintFlag) {
		this.tinyintFlag = tinyintFlag;
	}
}
