package dm.model;

import java.util.ArrayList;

/**
 * データモデル項目格納リストクラス
 * @author onodera
 *
 */
public class DataModelList {
	private ArrayList<DataModel> dataModelList;
	/**
	 * コンストラクタ
	 */
	public DataModelList(){
		this.dataModelList = new ArrayList<DataModel>();
	}

	/**
	 * 初期化
	 * @return
	 */
	public ArrayList<DataModel> dataModelList(){
		this.dataModelList = new ArrayList<DataModel>();
		return dataModelList;
	}
}
