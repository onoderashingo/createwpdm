package xml.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * XMLファイルダウンロードサーブレット
 * @author onodera
 *
 */
public class DMDownLoadServlet extends HttpServlet{

	public void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		OutputStream out = null;
		InputStream in =null;
		String filepath=null;
		try{
			request.setCharacterEncoding("UTF-8");
			response.setContentType("application/octet-stream");

			//ファイル名、パスを取得
			String fileName = (String)request.getAttribute("fileName");
			filepath = (String)request.getAttribute("filepath");

			//ダウンロードファイル取得時、日本語文字化け対策
			response.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode(fileName,"UTF-8")+"\"");

			//ダウンロードファイル読込
			in =new FileInputStream(filepath);

			//ダウンロード
			out = response.getOutputStream();
			byte[] buff = new byte[1024];
			int len = 0;
			while((len = in.read(buff, 0, buff.length)) != -1){
				out.write(buff,0,len);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			if(in != null){
				try{
					in.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
			if(out != null){
				try{
					out.close();
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
			Path path = Paths.get(filepath);
			Files.delete(path);
		}
	}

}
