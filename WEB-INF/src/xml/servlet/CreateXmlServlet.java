package xml.servlet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import dm.model.DataModel;
import xml.control.CreateXML;
import xml.control.TransformXML;

/**
 * データモデルxmlファイル生成サーブレット
 * @author onodera
 *
 */
public class CreateXmlServlet extends HttpServlet{
	public void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain; charset=UTF-8");
		String filepath = getServletContext().getRealPath("/files");
		Document doc=null;
		String fileName = null;
		try{
			//DM物理名、論理名、Alias取得
			//XMLファイル作成、DM基本情報登録
			DataModel dmName = new DataModel();
			dmName.setLogicName(request.getParameter("tableLogic"));
			dmName.setPhysicsName(request.getParameter("tablePhysics"));
			dmName.setAlias(request.getParameter("tableAlias"));
			ArrayList<Object> ret = new ArrayList<Object>();

			//XMLファイル生成。DM基本情報登録。
			ret = CreateXML.createXML(dmName);
			dmName = (DataModel)ret.get(0);
			doc = (Document)ret.get(1);
			Element dmlist = (Element)ret.get(2);
			Element datamodel = (Element)ret.get(3);
			fileName = (String)ret.get(4);

			//DM項目登録
			String nums[] = request.getParameterValues("num");
			for(int i = 0; i<nums.length;i++){
				DataModel dm = new DataModel();
				dm.setLogicName(request.getParameter("logicName"+nums[i]));
				dm.setPhysicsName(request.getParameter("physicsName"+nums[i]));
				dm.setAlias(request.getParameter("alias"+nums[i]));
				try{
				dm.setNullAble(request.getParameter("nullAble"+nums[i]));
				}
				catch(NullPointerException e){
					dm.setNullAble("false");
				}
				if(dm.getNullAble()!=null && dm.getNullAble().equals("◯")){
					dm.setNullAble("true");
				}
				else{
					dm.setNullAble("false");
				}
				try{
					dm.setPrimaryKey(request.getParameter("primaryKey"+nums[i]));
				}
				catch(NullPointerException e){
					dm.setPrimaryKey("");
				}
				if(dm.getPrimaryKey()!=null && dm.getPrimaryKey().equals("◯")){
					dm.setPrimaryKey("1");
				}
				else{
					dm.setPrimaryKey("0");
				}
				dm.setStringValue(Integer.parseInt(request.getParameter("stringValue"+nums[i])));
				dm.setDataType(request.getParameter("dataType"+nums[i]));
				dm.setTinyintFlag(Integer.parseInt(request.getParameter("tinyintFlag"+nums[i])));

				//データタイプがDATE,DATETIME,FILE,BOOLだった場合桁数に0をセット
				if("DATE".equals(dm.getDataType()) || "DATETIME".equals(dm.getDataType()) || "FILE".equals(dm.getDataType()) || "BOOL".equals(dm.getDataType())){
					dm.setStringValue(0);
				} else if(dm.getTinyintFlag()==1){
					dm.setStringValue(1);
				}
				//DM項目登録
				doc=CreateXML.columnXML(doc, dmlist, dm, dmName, i);
			}
			//targetAlias登録
			doc=CreateXML.aliasXML(doc, datamodel, dmName);
			File file = new File(filepath+"/"+fileName);

			//DOMツリーをxmlファイルに出力
			TransformXML.tfXML(doc, file);
			request.setAttribute("filepath", filepath+"\\"+fileName);
			request.setAttribute("fileName", fileName);
			RequestDispatcher rd = request.getRequestDispatcher("./DMDownLoadServlet");
			rd.forward(request, response);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
