package xml.control;

import java.io.File;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class TransformXML {
	/**
	 * DOMツリーXML変換メソッド
	 * @param doc データモデルドキュメント
	 * @param file データモデルファイル
	 * @throws TransformerException
	 */
	public static void tfXML(Document doc, File file) throws TransformerException {
		// DOMツリーをxmlファイルに出力
		Transformer tf = null;
		TransformerFactory tff = TransformerFactory.newInstance();
		try {
			tf = tff.newTransformer();
			tf.setOutputProperty("indent", "yes");
			tf.setOutputProperty("encoding", "UTF-8");
			tf.transform(new DOMSource(doc), new StreamResult(file));
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
	}
}
