package xml.control;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import dm.model.DataModel;

/**
 * データモデルドキュメント作成クラス
 *
 * @author onodera
 *
 */
public class CreateXML {
	final static String user = "CreateWPDM";
	static Date datetime = new Date();
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final static String create_date = sdf.format(datetime);

	/**
	 * XMLファイル作成、DM基本情報登録メソッド
	 *
	 * @param dmName
	 *            データモデルコードインスタンス
	 * @return データモデルコードインスタンス、XMLドキュメント、dm-item-list要素、XMLファイル名
	 */
	public static ArrayList<Object> createXML(DataModel dmName) {
		ArrayList<Object> object = new ArrayList<Object>();
		try {
			String DMcode = dmName.getLogicName();
			String fileName = DMcode + ".wprx";

			// ドキュメント作成
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			Document doc = builder.newDocument();
			Element datamodel = doc.createElement("dm");
			doc.appendChild(datamodel);

			// DMコード、物理名、登録
			createTitle(doc, datamodel, dmName.getLogicName(), dmName.getPhysicsName());
			// DM項目の親node登録
			Element dmlist = doc.createElement("dm-item-list");
			datamodel.appendChild(dmlist);

			object.add((Object) dmName);
			object.add((Object) doc);
			object.add((Object) dmlist);
			object.add((Object) datamodel);
			object.add((Object) fileName);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}

	// DMコード、名前登録メソッド
	/**
	 * DM共通項目登録メソッド
	 *
	 * @param doc
	 *            生成ファイル
	 * @param datamodel
	 *            親タグ要素
	 * @param tableLogic
	 *            DM論理名
	 * @param tablePhysics
	 *            DM物理名
	 * @return 編集したファイルを返す
	 */
	public static Document createTitle(Document doc, Element datamodel, String tableLogic, String tablePhysics) {
		append(doc, datamodel, "tool-version", "1.5.0.0");
		append(doc, datamodel, "code", tableLogic);
		append(doc, datamodel, "name", tablePhysics);
		append(doc, datamodel, "is-disable", "false");
		append(doc, datamodel, "creator", user);
		append(doc, datamodel, "create-time", create_date);
		append(doc, datamodel, "updator", user);
		append(doc, datamodel, "update-time", create_date);

		return doc;
	}

	/**
	 * データモデル項目登録メソッド
	 *
	 * @param doc
	 *            XMLドキュメント
	 * @param dmlist
	 *            dm-item-list要素
	 * @param dm
	 *            データモデル項目インスタンス
	 * @param dmName
	 *            データモデルコードインスタンス
	 * @param i
	 *            項目順番号
	 * @return XMLドキュメント
	 */
	public static Document columnXML(Document doc, Element dmlist, DataModel dm, DataModel dmName, int i) {
		// <dm>タグの子要素に<dm-item>タグを追加
		Element dmitem = doc.createElement("dm-item");
		dmlist.appendChild(dmitem);
		// <dm-item>タグの子要素にDM項目情報を追加
		append(doc, dmitem, "code", dm.getLogicName());
		append(doc, dmitem, "name", dm.getPhysicsName());
		append(doc, dmitem, "order", Integer.toString((i + 1) * 10));
		append(doc, dmitem, "length", Integer.toString(dm.getStringValue()));
		append(doc, dmitem, "byteSize", Integer.toString(0));
		append(doc, dmitem, "scale", Integer.toString(0));
		append(doc, dmitem, "is-nullable", dm.getNullAble());
		append(doc, dmitem, "key-group", dm.getPrimaryKey());
		append(doc, dmitem, "data-type", dm.getDataType());
		append(doc, dmitem, "is-disable", "false");
		append(doc, dmitem, "creator", user);
		append(doc, dmitem, "create-time", create_date);
		append(doc, dmitem, "updator", user);
		append(doc, dmitem, "update-time", create_date);
		Element item_prop_list = doc.createElement("dm-item-prop-list");
		dmitem.appendChild(item_prop_list);
		// columnAliasが登録されている時の処理
		if (dm.getAlias() != null && !(dm.getAlias().equals(""))) {

			Element dm_item_prop = doc.createElement("dm-item-prop");
			item_prop_list.appendChild(dm_item_prop);
			append(doc, dm_item_prop, "dm-code", dmName.getLogicName());
			append(doc, dm_item_prop, "dm-item-code", dm.getLogicName());
			append(doc, dm_item_prop, "key", "columnAlias");
			append(doc, dm_item_prop, "value", dm.getAlias());
			append(doc, dm_item_prop, "is-disable", "false");
		}
		return doc;
	}

	/**
	 * targetAlias登録メソッド
	 *
	 * @param doc
	 *            XMLドキュメント
	 * @param datamodel
	 *            dm要素
	 * @param dmName
	 *            データモデルコードインスタンス
	 * @return XMLドキュメント
	 */
	public static Document aliasXML(Document doc, Element datamodel, DataModel dmName) {
		append(doc, datamodel, "dm-ope-list", "");
		Element dm_prop_list = doc.createElement("dm-prop-list");
		datamodel.appendChild(dm_prop_list);
		// targetAliasが登録されている時の処理
		if (dmName.getAlias() != null && !(dmName.getAlias().equals(""))) {
			Element dm_prop = doc.createElement("dm-prop");
			dm_prop_list.appendChild(dm_prop);
			append(doc, dm_prop, "key", "tableAlias");
			append(doc, dm_prop, "value", dmName.getAlias());
			append(doc, dm_prop, "is-disable", "false");
		}
		return doc;
	}

	// XML要素追加メソッド

	/**
	 * xmlタグ追加メソッド
	 *
	 * @param doc
	 *            生成ファイル
	 * @param datamodel
	 *            親タグ要素
	 * @param ele
	 *            追加タグ 要素
	 * @param node
	 *            追加テキスト
	 * @return 編集したファイルを返す
	 */
	public static Document append(Document doc, Element datamodel, String ele, String node) {
		Element element = doc.createElement(ele);
		element.appendChild(doc.createTextNode(node));
		datamodel.appendChild(element);
		return doc;
	}
}
