WebPerformerで使用するデータモデルをSQL開発ツール「A5」で作成したテーブル定義書を読込み、
自動生成するWebアプリケーション。

実行環境

・Eclipse Mars 4.5.1

・Java version 1.8.0_91

・Apache Tomcat 8.0.26