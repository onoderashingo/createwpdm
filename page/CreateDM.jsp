<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="dm.*"%>
<%@ page import="dm.model.*"%>
<%@ page import="java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	HttpSession sess = request.getSession();
	DataModel dmName = new DataModel();

	DataModel datamodel = new DataModel();
	ArrayList<DataModel> dataModelList = new ArrayList<DataModel>();
	dmName = (DataModel) sess.getAttribute("dmName");
	dataModelList = (ArrayList<DataModel>) sess.getAttribute("dataModelList");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>データモデル作成</title>
<style type="text/css">
table {
	table-layout: fixed;
}

table.columnList {
	width: 1200px;
	table-layout: auto;
	word-wrap: break-word;
}

th.long, td.long, input.long {
	width: 250px;
}

td.small2, th.small2, input.small2 {
	width: 50px;
}

.notdisp {
	display: none;
}
</style>
<!-- BootstrapのCSS読み込み -->
<link href="<%=request.getContextPath()%>/src/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/src/script/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/src/script/createDM.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/src/script/bootstrap.min.js"></script>
	<div class="container">
		<h3 class="bg-info container">データモデル項目編集</h3>
		<!-- DMタイトル -->
		<form method="POST" name="dm_downLoad_form"
			action="../CreateXmlServlet">
			<table class="table table-striped table-bordered">
				<tr>
					<th class="long">DMコード・論理名</th>
					<th class="long">DM物理名</th>
					<th class="long">TargetAlias テーブル名</th>
				</tr>


				<tr>
					<td class="long"><input class="long" name="tableLogic"
						type="text" value="<%=dmName.getLogicName()%>"></td>
					<td class="long"><input class="long" id="tablePhysics"
						name="tablePhysics" type="text"
						value="<%=dmName.getPhysicsName()%>"></td>
					<td class="long"><input class="long" name="tableAlias"
						type="text" value="<%=dmName.getAlias()%>"></td>
				</tr>
			</table>
			<!-- DM項目リスト -->
			<table class="columnList table table-striped table-bordered">
				<tr>
					<!-- <th class="small">No</th> -->
					<th>項目コード・論理名</th>
					<th>項目物理名</th>
					<th>columnAlias カラム別名</th>
					<th class="small2" style="white-space: nowrap;">Null可</th>
					<th class="small2" style="white-space: nowrap;">主キー</th>
					<th class="small2">桁数</th>
					<th class="small2">データタイプ</th>
					<th></th>
				</tr>
				<%
					for (int i = 0; i < dataModelList.size(); i++) {
						datamodel = dataModelList.get(i);
				%>
				<tr class="column">
					<td class="long column"><input class="long"
						name="logicName<%=i + 1%>" type="text"
						value="<%=datamodel.getLogicName()%>"></td>
					<td class="long column"><input class="long physicsName"
						name="physicsName<%=i + 1%>" type="text"
						value="<%=datamodel.getPhysicsName()%>"></td>
					<td class="long column"><input class="long"
						name="alias<%=i + 1%>" type="text"
						value="<%=datamodel.getAlias()%>"></td>
					<td class="small2 column"><select class="small2"
						name="nullAble<%=i + 1%>">
							<option value=""
								<%="".equals(datamodel.getNullAble()) ? "selected=\"selected\"" : ""%>></option>
							<option value="◯"
								<%="◯".equals(datamodel.getNullAble()) ? "selected=\"selected\"" : ""%>>◯</option>
					</select></td>
					<td class="small2 column"><select class="small2"
						name="primaryKey<%=i + 1%>">
							<option value=""
								<%="".equals(datamodel.getPrimaryKey()) ? "selected=\"selected\"" : ""%>></option>
							<option value="◯"
								<%="◯".equals(datamodel.getPrimaryKey()) ? "selected=\"selected\"" : ""%>>◯</option>
					</select></td>
					<td class="small2 column"><input class="small2"
						name="stringValue<%=i + 1%>" type="text"
						value="<%=datamodel.getStringValue()%>"></td>
					<td class="small2 column"><select class="small2"
						name="dataType<%=i + 1%>">
							<option value=""
								<%="".equals(datamodel.getDataType()) ? "selected=\"selected\"" : ""%>></option>
							<option value="CODE"
								<%="CODE".equals(datamodel.getDataType()) ? "selected=\"selected\"" : ""%>>CODE</option>
							<option value="TEXT"
								<%="TEXT".equals(datamodel.getDataType()) ? "selected=\"selected\"" : ""%>>TEXT</option>
							<option value="NUM"
								<%="NUM".equals(datamodel.getDataType()) ? "selected=\"selected\"" : ""%>>NUM</option>
							<option value="CURRENCY"
								<%="CURRENCY".equals(datamodel.getDataType()) ? "selected=\"selected\"" : ""%>>CURRENCY</option>
							<option value="DATE"
								<%="DATE".equals(datamodel.getDataType()) ? "selected=\"selected\"" : ""%>>DATE</option>
							<option value="TIME"
								<%="TIME".equals(datamodel.getDataType()) ? "selected=\"selected\"" : ""%>>TIME</option>
							<option value="BOOL"
								<%="BOOL".equals(datamodel.getDataType()) ? "selected=\"selected\"" : ""%>>BOOL</option>
							<option value="FILE"
								<%="FILE".equals(datamodel.getDataType()) ? "selected=\"selected\"" : ""%>>FILE</option>
					</select></td>
					<td class="column"><input type="button" name="delete"
						class="delete btn btn-default" value="項目除去">
					<td class="small notdisp"><input class="small" name="tinyintFlag<%=i+1 %>" type="hidden" value=<%=datamodel.getTinyintFlag() %>>>
					<td class="small notdisp"><input class="small" name="num"
						type="hidden" value="<%=i + 1%>">
				</tr>
				<%
					}
				%>
			</table>
			<input type="submit" id="dm_downLoad" name="dm_downLoad"
				value="DMダウンロード" class="btn btn-default btn-success"> <input
				type="button" name="back" onClick='history.back();' value="戻る"
				class="btn btn-default" style="margin-left:50px;">
		</form>
	</div>
</body>
<footer><br></footer>
</html>