<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="javax.servlet.*"%>
<%@ page import="table.model.*"%>
<%
	HttpSession sess = request.getSession();
	TableName tableName = new TableName();
	ArrayList<TableName> tableList = new ArrayList<TableName>();
	Column column = new Column();
	ArrayList<Column> columnList = new ArrayList<Column>();
	Map<String, ArrayList<Column>> tableMap = new LinkedHashMap<String, ArrayList<Column>>();
	tableList = (ArrayList<TableName>) sess.getAttribute("tableList");
	tableMap = (Map<String, ArrayList<Column>>) sess.getAttribute("tableMap");
	columnList = (ArrayList<Column>) sess.getAttribute("columnList");
	int i = 0;
	String tableLogic = null;
	String tablePhysics = null;
	String targetKey = null;
	String selected = null;

	targetKey = (String) sess.getAttribute("targetKey");
	//Set<String> keyset = tableMap.keySet();
	if (tableMap != null) {
		Set<Map.Entry<String, ArrayList<Column>>> set = tableMap.entrySet();
		columnList = tableMap.get(targetKey);
		sess.setAttribute("columnList", columnList);
	}
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WPDM作成</title>
<meta http-equiv="Content-Style-Type" content="text/css">
<!-- BootstrapのCSS読み込み -->
<link href="<%=request.getContextPath()%>/src/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
th, td {
	width: 200px;
}

.notedit {
	pointer-events: none;
	width: 200px;
}

.table>tbody>tr>td.hidden-border {
	display: none;
}
</style>
</head>
<body>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/src/script/jquery/jquery-1.9.1.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/src/script/colmunChange.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/src/script/jquery/jquery.json-2.4.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/src/script/bootstrap.min.js"></script>
	<div class="container">
		<h3 class="bg-info container">テーブル定義ファイルアップロード</h3>
		<!-- テーブル定義書.xlsxアップロード -->
		<form method="POST" enctype="multipart/form-data"
			action="../TableServlet">
			<input type="file" class="well-sm" size="60" name="TableFile"
				id="tableFile"> <button class="btn btn-default btn-primary"
				name="file_btn" id="file_btn"><i class="glyphicon glyphicon-file">表示</i></button>

		</form>
		<br>
		<!-- テーブル定義書論理名、物理名 -->
		<form method="POST" name="CreateDM_form" action="../CreateDMServlet">
			<table class="table-bordered">
				<tr>
					<%
						if (tableList != null || tableMap != null) {
					%>
					<td class="well-sm">論理エンティティ名</td>
					<td><select id="tableLogic" name="TableLogic"
						class="form-control-static">
							<%
								for (i = 0; i < tableList.size(); i++) {
										tableName = tableList.get(i);
										tableLogic = tableName.getTableLogic();
							%>
							<option value=<%=tableName.getTableLogic()%>>
								<%=tableName.getTableLogic()%>
							</option>
							<%
								}
							%>
					</select></td>
					<td class="well-sm">物理エンティティ名</td>
					<td><select id="tablePhysics" name="TablePhysics"
						class="form-control-static">
							<%
								for (i = 0; i < tableList.size(); i++) {
										tableName = tableList.get(i);
										tablePhysics = tableName.getTablePhysics();
							%>
							<option value=<%=tableName.getTablePhysics()%>><%=tableName.getTablePhysics()%></option>
							<%
								}
							%>
					</select></td>
					<%
						}
					%>
				</tr>
			</table>
			<input type="hidden" id="tableNo" name="TableNo" value="">
			<!-- カラム情報リスト -->
			<table class="table table-striped table-bordered">
				<tr>
					<!--<th>No</th>-->
					<th>論理名</th>
					<th>物理名</th>
					<th>データ型</th>
					<th>NOT NULL</th>
					<th>備考</th>
				</tr>
				<%
					if (tableList != null && tableMap != null) {
						for (int l = 0; l < columnList.size(); l++) {
							column = columnList.get(l);
				%>
				<tr>
					<td><input class="notedit" name="logicName<%=l + 1%>"
						type="text" value="<%=column.getLogicName()%>"></td>
					<td><input class="notedit" name="physicsName<%=l + 1%>"
						type="text" value="<%=column.getPhysicsName()%>"
						class="physicsName"></td>
					<td><input class="notedit" name="dataType<%=l + 1%>"
						type="text" value="<%=column.getDateType()%>"></td>
					<td><input class="notedit" name="notNull<%=l + 1%>"
						type="text" value="<%=column.getNotNULL()%>"></td>
					<td><input class="notedit" name="notes<%=l + 1%>" type="text"
						value="<%=column.getNotes()%>"></td>
					<td class="hidden-border"><input class="notedit" name="num"
						type="hidden" value=<%=l + 1%>></td>
				</tr>
				<%
						}
					}
				%>
			</table>
			<input type="submit" id="create_btn" class="btn btn-default btn-success"
				name="create_btn" value="DM表示"> <input type="button"
				name="top" class="btn btn-default"
				onClick="location.href='<%=request.getContextPath()%>/index.html'"
				value="TOP" style="margin-left:50px;">
		</form>
		<%--	<h3>DDL</h3> --%>
		<%--	<textarea style="pointer-events: none">ddl表示</textarea> --%>
		<%--	<br> --%>
		<%--	<input type="submit" name="ddl_btn" value="DDL実行"> --%>
	</div>
</body>
<footer><br></footer>
</html>