/**
 *DM項目リスト削除ボタン処理
 *DMダウンロードボタン制限処理
 */
$(function($){
	var tablePhysics = $('#tablePhysics');

	//行削除ボタン
	$('.delete').on('click',function(){
		$(this).parent().parent().remove();
	});
	//DMダウンロードボタン
	$('#dm_downLoad').on('click',function(){
		var physicsName = $('.physicsName');
		var columnList = new Array();
		columnList = $('tr.column');
		//項目が存在しない処理
		if(columnList.length == null || columnList.length == 0){
			$(this).get(0).type = "button";
			alert("項目がありません。\nテーブルを選択し直してください。");
			return false;
		}

		//DM物理名が空の処理
		if(tablePhysics.val() == null || tablePhysics.val() == ""){
			$(this).get(0).type = "button";
			alert("DM物理名が入力されていません。\nDM物理名を入力してください");
		}
		else{
			$(this).get(0).type = "submit";
		}

		//物理名が空の処理
		physicsName.each(function(key,value){
			if($(value).val()==null||$(value).val()==""){
				$('#dm_downLoad').get(0).type = "button";
				alert("物理名が入力されていません。\n物理名を入力してください");
				return false;
			}
			else{
				return true;
			}
		});
	});
});