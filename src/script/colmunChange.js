/**
 * UpLoad.jsp内のテーブル物理エンティティ名、論理エンティティ名が変更された処理 物理エンティティ名と論理エンティティ名のセレクトボックスを連動
 * 選択された物理エンティティ名をJsonServletに渡す
 */
$(function($) {
	var $tableLogic = $('#tableLogic');
	var $tablePhysics = $('#tablePhysics');
	var $tableNo = $('#tableNo');
	var $tableFile = $('#tableFile');

	var $columnPhysicsList = new Array();
	$columnPhysicsList = $('.physicsName');
	var search = location.search;
	// 論理名、物理名変更時リスト選択
	// 再読み込み時、searchが存在していればそのindexのセレクトボックスを選択
	if( search != null && search != ""){
		$tableLogic.children().eq(search.slice(7)).prop('selected',"selected");
		$tablePhysics.children().eq(search.slice(7)).prop('selected',"selected");
	}
	/**
	 * xlsxファイル以外のファイルが選択された処理
	 */
	$tableFile.on('change',function(){
		var file = this.files[0];
		if(file.type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
			alert("xlsxファイル以外が選択されています。");
			$(this).val(null);
		}
	});


	/**
	 * テーブル定義書未選択時表示ボタンの処理
	 *
	 */
	$('#file_btn').on('click',function(){
		if($tableFile.val() === null || $tableFile.val() === ""){
			$(this).get(0).type = "button";
			alert("テーブル定義書を選択してください。");
		}
		else{
			$(this).get(0).type = "submit";
		}
	});

	/**
	 * 論理テンティティ名が変更された処理
	 */
	$tableLogic.change(function() {
		// テーブル物理名と連動
		// 選択されたindexを取得
		var ele_logic = $(this).prop('selectedIndex');
		// 物理エンティティ名にindexをセット
		$tablePhysics.children().eq(ele_logic).prop('selected',"selected");
		$tableNo.val(ele_logic);
		// 変更された物理エンティティ名をtargetKeyにセット
		var targetKey = $tablePhysics.val()
		// JsonServlet呼び出し
		$.ajax({
			type : 'POST',
			url : '../JsonServlet',
			dataType : 'text',
			data : {
				targetKey : targetKey
			},
			success : function() {
				// URLにvalue=indexを追加して遷移
				location.href = "./UpLoad.jsp?value="+ele_logic;
			}
		});
	});
	/**
	 *  物理エンティティ名が変更された処理
	 */
	$tablePhysics.change(function() {
		// テーブル論理名と連動
		// 選択されたindexを取得
		var ele_physics = $(this).prop('selectedIndex');
		$tableLogic.children().eq(ele_physics).prop('selected',"selected");
		$tableNo.val(ele_physics);
		// 変更した物理エンティティ名をtagetKeyにセット
		var targetKey = $(this).val();
		// JsonServlet呼び出し
		$.ajax({
			type : 'POST',
			url : '../JsonServlet',
			dataType : 'text',
			data : {
				targetKey : targetKey
			},
			success : function() {
				// URLにvalue=indexを追加して遷移
				location.href = "./UpLoad.jsp?value="+ele_physics;
			}
		});
	});
	/**
	 *  物理エンティティ、物理名が空の処理
	 */
	$('#create_btn').on('click',function(){
		if($('#tablePhysics option:selected').val() == null || $('#tablePhysics option:selected').val() == ""){
			$(this).get(0).type = "button";
			alert("物理エンティティ名が登録されていません。\nテーブル定義書ファイルを修正してください。");
		}
		else{
			$(this).get(0).type = "submit";
		}
		$columnPhysicsList.each(function(key,value){
			if($(value).val() == null || $(value).val() == ""){
				$('#create_btn').get(0).type = "button";
				alert("物理名が登録されていません。\nテーブル定義書ファイルを修正してください。");
				return false;
			}
			else{
				return true;
			}
		});
	});
});