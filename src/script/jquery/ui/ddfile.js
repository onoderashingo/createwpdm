(function($) {
	$.fn.ddfile = function(options) {
		/* 引数の初期値を設定（カンマ区切り） */
		var defaults = {
			contextPath: null,
			// 文字列系
			message: {
				btnTxt: "ファイルを選択",
				clearBtnTxt: "ファイルクリア",
				notSelectTxt: "選択されていません",
				plzDropTxt: "ここにファイルをドロップしてください。",
				sizeOverTxt: "容量が大きすぎます",
			},
			// 数値系
			maxSize:-1, // 最大アップロード可能容量（byte）
			// フィールド
			idFieldName:null,
			nameFieldName:null,
			styleClass: "",
			ioDispLength: 0,
			group:null,
			rowId:null,
		};
		var options = $.extend(defaults, options);

		var fileElem = this;
		// fileElem.selector だとidになる。idだと一覧でオブジェクトがとれない。
		var fileElemSelector = "input[name='" + fileElem.attr("name") + "']";

		// 部品一式
		var wrapper = $('<div/>').addClass('ddfile-wrapper');
		var droppable = $('<div id=' +  fileElem.attr("id") + '_wrapper/>').addClass(defaults.styleClass);
		var btn = $('<input type="button" value=' + defaults.message.btnTxt + '>');
		if (defaults.group) {
			btn.addClass('GROUP_BUTTON');
		}
		var clearBtn = $('<input type="button" value=' + defaults.message.clearBtnTxt + '>');
		if (defaults.group) {
			clearBtn.addClass('GROUP_BUTTON');
		}
		var txt = $('<span>' + defaults.message.notSelectTxt + '</span>');
		var msg = $('<div class="dd-msg">' + defaults.message.plzDropTxt + '</div>');

		wrapper.css({
			'display' : 'inline-block',
		});
		droppable.css({
			'display' : 'inline-block',
		});
		txt.css({
			'padding-right' : 10,
		});
		if (defaults.ioDispLength > 0) {
			txt.css({
				'display' : 'inline-block',
				'overflow' : 'hidden',
 				'vertical-align' : 'middle',
				'width' : defaults.ioDispLength + 'em',
			});
		}
		btn.css({
		});
		clearBtn.css({
			'display' : 'none'
		});
		fileElem.css({
			'display' : 'none'
		});
		msg.css({
			'display' : 'none'
		});

		fileElem.before(wrapper);
		wrapper.append(fileElem);
		wrapper.append(droppable);
		droppable.append(txt).append(btn).append(clearBtn).append(msg);

		/*
		 * イベントキャンセル
		 */
		var cancelEvent = function(event) {
			(event.preventDefault) ? event.preventDefault():event.returnValue=false;
			(event.stopPropagation) ? event.stopPropagation():event.returnValue=false;
		}

		/*
		 * 選択時の動き
		 */
		var handleInpuFile = function(event) {
			var files = $(this).prop('files');
			_ddfileUpload(event, files);
		}

		fileElem.bind('change', handleInpuFile);

		/*
		 * input type=file クリック紐付
		 */
		var handleClick = function() {
			$(fileElemSelector).click();
		}

		/*
		 * ドロップ時の動き
		 */
		var handleDroppedFile = function(event) {
			cancelEvent(event);
			var files = event.originalEvent.dataTransfer.files;
			_ddfileUpload(event, files);
		}

		/*
		 * ファイル名（重さ付き）取得
		 */
		var getFileLabel = function(fileListSet) {
			var str = fileListSet[0].name;
			var prefixStr = "";

			if (fileListSet.length > 1) {
				// ファイルを１つにしてから呼ばれるので、複数ファイルになることはない
				str += ' 他' + (fileListSet.length - 1) + 'ファイル';
				prefixStr = "計";
			}
			var totalSize = sumSize(fileListSet);
			str += ' (' + prefixStr + Math.ceil(totalSize / 100) / 10 + 'KB)';

			return str;
		}

		/*
		 * サイズチェック
		 */
		// return ture:fileSize is over
		var checkSize = function(totalSize, maxSize) {
			if (maxSize > 0 && $.type(defaults.maxSize) === "number") {
//				console.log((Math.ceil(totalSize / 100) / 10) + " / " + defaults.maxSize);
				return (totalSize > defaults.maxSize);
			}
			return false;
		}

		/*
		 * ファイル合計
		 */
		var sumSize = function(sumFileList) {
			var sums = 0;
			for (i = 0; i < sumFileList.length; i++) {
				sums += sumFileList[i].size;
			}
			return sums;
		}

		// 対象要素と子要素とでの移動フラグ(chrome対策)
		var innerFlag = false;

		/*
		 * Drop領域開く
		 */
		var scallup = function(event) {
			if (event.type == "dragenter") {
				//フラグをセット
				innerFlag = true;
			} else {	// event.type == "dragover"
				//フラグを「握りつぶ」す
				innerFlag = false;
				//クラスをセット
				droppable.addClass('scallup');
				msg.css('display', '');
			}

			cancelEvent(event);
		}

		/*
		 * Drop領域閉じる
		 */
		var scalldown = function(event) {
			if (innerFlag) {
				//フラグがセットされている場合、フラグを戻す
				innerFlag = false;
			} else if (!droppable.hasClass("ddfile__ERROR")) {
				droppable.removeClass('scallup');
				msg.css('display', 'none');
			}
			cancelEvent(event);
		}

		var _ddfileUpload = function(event, files) {
			if (files.length < 1) {
				scalldown(event);
				return;
			}
			// 強制的に1ファイルに
			files = jQuery.grep(files, function(value, n) {
				return (n == 0);
			});

			// 重さチェック
			// TODO 正常時、異常時は、クラスを変更するようにし、クラスに対してCSSを設定する
			if (checkSize(sumSize(files), defaults.maxSize)) {
				_ddfileSetError(defaults.message.sizeOverTxt, getFileLabel(files));
				return;
			} else if (files[0].size == 0) {
				// ファイルサイズ0はサーバーでテンポラリファイルが作られないため、受け付けないようにする
				scalldown(event);
				return
			}

			// スクリーンロック
			_ddfileScreenLock(true);

			var fileReader = new FileReader();
			fileReader.readAsText(files[0]);

			// ファイル読み込み失敗時処理（フォルダD&D時もエラーが発生する）
			fileReader.onerror = function(event) {
				// スクリーンロック解除
				_ddfileScreenLock(false);
				scalldown(event);
			}

			// ファイル読み込み成功時処理
			fileReader.onload = function(event) {
				// ファイルをアップロードする
				var fd = new FormData(); // multipartデータとしてファイルを送信する
				fd.append("ddfile", files[0]);
				$.ajax({
					type : 'POST',
					url : defaults.contextPath + '/_dragdropUpload.do',
					data : fd,
					contentType: false,
					processData: false,
					async: false,
					dataType: 'json', // 戻り値はjson形式で受け取る
					success : function(data) {
						// ファイル項目リセット
						$("input[name='" + defaults.idFieldName + "']").attr("value", "");
						_ddfileClear();

						if (data.message != "") {
							// エラー時
							_ddfileSetError(data.message, getFileLabel(files));
							return;
						}

						// ファイルIDをhiddesnフィールドに格納する
						$("input[name='" + defaults.idFieldName + "']").attr("value", data.id);
						$("input[name='" + defaults.nameFieldName + "']").attr("value", files[0].name);

						// 更新フラグセット
						if (defaults.group) {
							if ($("input[name='"+ defaults.group + "[" + defaults.rowId + "]._status']").attr("value") == "") {
								$("input[name='"+ defaults.group + "[" + defaults.rowId + "]._status']").attr("value", "UPDATE");
								$("span[name='"+ defaults.group + "[" + defaults.rowId + "]._statusbtn']").attr("class", "UPDATE");
							}
						} else {
							if (getForm()._status.value == "") {
								getForm()._status.value = "UPDATE";
							}
						}

						txt.text(getFileLabel(files));

						//クリアボタン表示
						_ddfileSetDisplayClearButton();

						// 連携対象となっていたら from全体をsubmitする
						fieldName = fileElem.attr('name');
						if (existField(fieldName, getChioceRefField())
							|| existField(fieldName, getDefalutValueRefField())
							|| existField(fieldName, getStatementRefField())
							|| existField(fieldName, getConditionRefField())
							|| existField(fieldName, getFieldTypeRefField())) {
							_ddfileScreenLock(false);
							buttonAction("event_FieldChange", defaults.idFieldName, _globalMsgObject.doRedraw, defaults.rowId);
						}
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						alert('ファイルのアップロードに失敗しました:');
					},
					complete :function(XMLHttpRequest, textStatus){
						// スクリーンロック解除
						_ddfileScreenLock(false);
					}
				});
			}
		}

		/*
		 * アップロード前・後処理
		 */
		var _ddfileScreenLock = function(isLock) {
			if (isLock) {
				setScreenLock(true);
				document.body.style.cursor = "wait";
				disp_ScreenLock();
				sendMsg();
				showStatusBar(_globalMsgObject.sending);
			} else {
				document.body.style.cursor = "";
				setScreenLock(false);
				clearSendMsg();
				clear_ScreenLock();
				showStatusBar("");
			}
		}

		/**
		 * ファイルクリアボタン処理
		 */
		var _ddfileClear = function (e) {
			var value = $("input[name='" + defaults.idFieldName + "']").attr("value");
			if (value) {
				// サーバーファイルのクリア
				$.ajax({
					type : 'POST',
					url : defaults.contextPath + '/_dragdropClear.do',
					data : {'ddfileId' : value},
					success : function(data) {
//						console.log("success")
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						// 失敗してもセッション切れ時にクリアされる
//						console.log("error")
					}
				});
			}

			// inputフィールドオブジェクトをクリアするためにHTMLを置き換える
			// （outerHTML だけだと firefox以外でvalue がクリアされない
			$(fileElemSelector).replaceWith($(fileElemSelector).clone() );
			$(fileElemSelector).bind('change', handleInpuFile);
			try {
				// firefoxでvalueがクリアされない
				if ($(fileElemSelector).attr("value")) {
					// ヌル文字のみセットできる(念のためtry～catchで囲む)
					$(fileElemSelector).attr("value", "");
				}
			} catch(e) {}

			// hiddean のクリア
			$("input[name='" + defaults.idFieldName + "']").attr("value", "");
			$("input[name='" + defaults.nameFieldName + "']").attr("value", "");

			// テキスト のクリア
			txt.text(defaults.message.notSelectTxt);
			msg.css('color', '');
			droppable.css('background-color', '');
			droppable.removeClass("ddfile__ERROR");
			scalldown(event);
			msg.html(defaults.message.plzDropTxt);

			_ddfileSetDisplayClearButton();
		}

		/**
		 * エラー処理
		 */
		var _ddfileSetError = function (messageStr, fileLabel) {
			// スクリーンロック解除
			_ddfileScreenLock(false);

			// ファイル項目リセット
			_ddfileClear();

			//エラー表示
			msg.css('display', '');
			msg.css('color', 'red');
			droppable.css('background-color', '#fcc');
			droppable.addClass("ddfile__ERROR");
			msg.html(messageStr);
			cancelEvent(event);
			txt.text(fileLabel);

			//クリアボタン表示
			_ddfileSetDisplayClearButton();
		}

		/**
		 * クリアボタン表示／非表示処理
		 */
		var _ddfileSetDisplayClearButton = function () {
			if ($("input[name='" + defaults.idFieldName + "']").attr("value") != "" || droppable.hasClass("ddfile__ERROR")) {
				clearBtn.css('display', '');
			} else {
				clearBtn.css('display', 'none');
			}
		}

		txt.bind('click', handleClick);
		btn.bind('click', handleClick);
		clearBtn.bind('click', _ddfileClear);

		droppable.bind('dragenter', scallup);
		droppable.bind('dragover', scallup);
		droppable.bind('dragleave', scalldown);

		droppable.bind('drop', handleDroppedFile);

		return this;
	};
})(jQuery);
